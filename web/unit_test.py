import json
import unittest

import pytest

import web.app
import web.dal


@pytest.fixture
def app():
    app_instance = web.app.create_app()
    web.app.register_routes(app_instance)
    app_instance.testing = True
    return app_instance.test_client()


@pytest.fixture
def dal():
    return web.dal.Dal()


@pytest.fixture()
def fill_db():
    d = dal()
    print "{} documents removed".format(d.clear_all_posts())
    for i in range(100):
        d.create("Test Post {}".format(i))


class PostsTestCreate(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_Create(self):
        post_text = dict(post="This is my first post!")
        response = app().post('/v0/post', data=json.dumps(post_text), content_type='application/json')
        self.assertEqual(response.status_code, 200)

    def test_Create_empty(self):
        response = app().post('/v0/post', data=json.dumps(""), content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_Create_no_body(self):
        response = app().post('/v0/post')
        self.assertEqual(response.status_code, 400)


class PostsUpdateTest(unittest.TestCase):
    def test_Update(self):
        d = dal()
        rec_id = d.create("Post for update unit test")
        post_text = json.dumps(dict(post="This is my updated update post!"))
        url = "/v0/post/{}".format(str(rec_id))
        print url
        response = app().put(url, data=post_text, content_type='application/json')
        self.assertEqual(response.status_code, 200)

    def test_Update_not_exists(self):
        post_text = dict(post="This is my updated post!")
        response = app().put('/v0/post/1', data=json.dumps(post_text), content_type='application/json')
        self.assertEqual(response.status_code, 400)

    def test_Update_failure(self):
        post_text = dict(post="This is my updated post!")
        response = app().put('/v0/post/100', data=json.dumps(post_text), content_type='application/json')
        self.assertEqual(response.status_code, 400)


class PostsUpAndDnVoteTest(unittest.TestCase):
    def setUp(self):
        d = dal()
        self.create_id = d.create("UpVote Test")

    def test_Upvote_not_exists(self):
        response = app().post('/v0/upvote/1')
        self.assertEqual(response.status_code, 400)

    def test_Upvote(self):
        rec = dal().get(self.create_id)
        rec_id = str(rec['_id'])
        for i in range(3):
            response = app().post('/v0/upvote/{}'.format(rec_id))
            self.assertEqual(response.status_code, 200)

    def test_Dnvote_not_exists(self):
        response = app().post('/v0/downvote/1')
        self.assertEqual(response.status_code, 400)

    def test_Dnvote(self):
        rec = dal().get(self.create_id)
        rec_id = str(rec['_id'])
        for i in range(3):
            response = app().post('/v0/downvote/{}'.format(rec_id))
            self.assertEqual(response.status_code, 200)


class PostsTopListTest(unittest.TestCase):
    def setUp(self):
        import random
        d = dal()
        d.clear_all_posts()
        for i in range(5):
            post_id = d.create("TopList Test Post")
            for v in range(random.randrange(0, 100)):
                d.up_vote(post_id)

    def test_Toplist(self):
        self.response = app().get('/v0/topstories')
        self.assertEqual(self.response.status_code, 200)


class PostsImportTest(unittest.TestCase):
    def test_Import(self):
        self.response = app().get('/v0/import')
        self.assertEqual(self.response.status_code, 200)


class PostsGetTest(unittest.TestCase):
    def test_Get(self):
        d = dal()
        post_id = d.create("Get Test Post")
        response = app().get('/v0/post/{}'.format(post_id))
        self.assertEqual(response.status_code, 200)


if __name__ == "__main__":
    unittest.main()
