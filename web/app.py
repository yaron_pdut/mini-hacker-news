import json
import requests
import itertools
from flask import Flask, request
from dal import Dal


def create_app():
    return Flask("PostsApp")


err_messages = {
    'OK': ['success', 200],
    'ERROR': ['error/failure', 400],
    'STORAGE_ERR': ['error while inserting record to storage', 400],
    'NOT FOUND': ['post not found', 400],
    'NO_BODY': ['no body in post request', 400],
    'NO POST': ['no post record in request', 400],
    'NO ID': ['No post id in args', 400],
    'EMPTY BODY': ['empty or wrong type body content(json body type?)', 400],
    'DB EMPTY': ['No posts in database', 400]
}


def http_response(status_code):
    return json.dumps({"msg": err_messages[status_code][0]}), err_messages[status_code][1], {
        'ContentType': 'application/json'}


def register_routes(app):
    @app.route('/v0/post', methods=['POST'])
    def api_post_new():
        post_body = request.get_json(silent=True)
        if post_body is None or post_body is u'':
            return http_response('NO_BODY')
        post_text = post_body.get('post')
        if post_text is None or post_text is '':
            return http_response('NO POST')

        dal = Dal()
        if not dal.create(post_text):
            return http_response('STORAGE_ERR')

        return http_response('OK')

    @app.route('/v0/post/<string:post_id>', methods=['GET'])
    def api_post_get(post_id):
        if post_id is None:
            return http_response('NO ID')

        dal = Dal()

        r = dal.get(post_id)
        if r is not None:
            response = {"date": r['date'], "post": r['post']}
            return json.dumps(response), 200, {'ContentType': 'application/json'}

        return http_response('NO FOUND')

    @app.route('/v0/post/<string:post_id>', methods=['PUT'])
    def api_posts_update(post_id):

        print post_id
        content = request.get_json(silent=True)

        if request.data is None or type(content) is not dict:
            return http_response('EMPTY BODY')

        if post_id is None:
            return http_response('NO ID')

        post_text = content.get('post')
        if post_text is None:
            return http_response('NO POST')

        dal = Dal()
        if not dal.update(post_id, post_text):
            return http_response('STORAGE_ERR')

        return http_response('OK')

    @app.route('/v0/upvote/<string:post_id>', methods=['POST'])
    def api_up_vote(post_id):
        if post_id is None:
            return http_response('NO ID')
        dal = Dal()
        if dal.up_vote(post_id):
            return http_response('OK')
        else:
            return http_response('ERROR')

    @app.route('/v0/downvote/<string:post_id>', methods=['POST'])
    def api_down_vote(post_id):
        if post_id is None:
            return http_response('NO ID')
        dal = Dal()
        if dal.down_vote(post_id):
            return http_response('OK')
        else:
            return http_response('ERROR')

    @app.route('/v0/topstories', methods=['GET'])
    def api_top_list():
        dal = Dal()
        data_set = dal.top_list()
        if len(data_set) is 0:
            return http_response('DB EMPTY')

        return json.dumps(data_set), 200, {'ContentType': 'application/json'}

    @app.route('/v0/h', methods=['GET'])
    def api_hello():
        return "\n<H>Hello, World!<H>\n"

    @app.route('/v0/import', methods=['GET'])
    def import_from_hackernews():
        r = requests.get('https://hacker-news.firebaseio.com/v0/topstories.json')
        items = json.loads(r.text)
        dal = Dal()
        for item in list(itertools.islice(items, 10)):
            r1 = requests.get('https://hacker-news.firebaseio.com/v0/item/' + str(item) + '.json')
            item_js = json.loads(r1.text)
            dal.create(post_text=item_js['title'], rank=item_js['score'])
        return http_response("OK")


if __name__ == '__main__':
    the_app = create_app()
    register_routes(the_app)
    the_app.run(debug=True, host='0.0.0.0')
