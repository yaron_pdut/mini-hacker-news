import os
from datetime import datetime
from bson import objectid, errors
from pymongo import DESCENDING, MongoClient


def front_page_rank(score, sock_votes, age, gravity=1.4, timebase=120):
    """
    Hacker News Ranking Algorithm
    http://sangaline.com/post/reverse-engineering-the-hacker-news-ranking-algorithm/
    """
    effective_score = score - sock_votes - 1
    return effective_score / ((timebase + age) / 60) ** gravity


def in_docker():
    """ Returns: True if running in a docker container, else False """
    if not os.path.isfile('/proc/1/cgroup'):
        return False
    with open('/proc/1/cgroup', 'rt') as ifh:
        return 'docker' in ifh.read()


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Dal(object):
    __metaclass__ = Singleton

    __slots__ = ['_client', '_db']

    def __init__(self):
        if in_docker():
            self._client = MongoClient("db")
        else:
            self._client = MongoClient()

        self._db = self._client.postdb

    def clear_all_posts(self):
        result = self._db.postdb.delete_many({})
        return result.deleted_count

    def create(self, post_text, rank=0):
        time_stamp = datetime.utcnow()
        time_stamp_in_sec = time_stamp - datetime(1970, 1, 1)

        item_doc = {
            "date": time_stamp.strftime("%Y-%m-%d_%H:%M:%S"),
            "post": post_text,
            "up_vote": rank,
            "down_vote": 0,
            "rank": front_page_rank(0, 0, (time_stamp_in_sec.seconds // 60) % 60)
        }
        return self._db.postdb.insert_one(item_doc).inserted_id

    def update(self, post_id, update_text):
        if self.get(post_id) is None:
            return False
        res = self._db.postdb.update_one({'_id': objectid.ObjectId(str(post_id))}, {'$set': {'post': update_text}}, upsert=False)
        return res.modified_count > 0

    def get_one(self):
        try:
            return self._db.postdb.find_one({})
        except errors.InvalidId:
            return None

    def get(self, post_id):
        try:
            return self._db.postdb.find_one({"_id": objectid.ObjectId(str(post_id))})
        except errors.InvalidId:
            return None

    def up_vote(self, post_id):
        item = self.get(post_id)
        if item is None:
            return False

        now = datetime.utcnow()
        past = datetime.strptime(item['date'], "%Y-%m-%d_%H:%M:%S")
        td = now - past
        rank = front_page_rank(item['up_vote'] + 1, item['down_vote'], (td.seconds // 60) % 60)

        res = self._db.postdb.update_one({'_id': objectid.ObjectId(post_id)},
                                         {'$inc': {'up_vote': 1},
                                          '$set': {'rank': rank}
                                          }, upsert=False)
        return res.modified_count > 0

    def down_vote(self, post_id):
        item = self.get(post_id)
        if item is None:
            return False

        now = datetime.utcnow()
        past = datetime.strptime(item['date'], "%Y-%m-%d_%H:%M:%S")
        td = now - past
        rank = front_page_rank(item['up_vote'] + 1, item['down_vote'], (td.seconds // 60) % 60)

        res = self._db.postdb.update_one({'_id': objectid.ObjectId(post_id)},
                                         {'$inc': {'down_vote': 1},
                                          '$set': {'rank': rank}
                                          }, upsert=False)
        return res.modified_count > 0

    def top_list(self, num_of_posts=100):
        now = datetime.utcnow()

        for item in self._db.postdb.find(sort=[('rank', DESCENDING)], limit=100):
            past = datetime.strptime(item['date'], "%Y-%m-%d_%H:%M:%S")
            td = now - past
            new_rank = front_page_rank(item['up_vote'], item['down_vote'], (td.seconds // 60) % 60)
            res = self._db.postdb.update_one({'_id': item['_id']}, {'$set': {'rank': new_rank}}, upsert=False)
            if res.modified_count < 0:
                print "Error while updating rank"

        top_list = []
        for item in self._db.postdb.find(sort=[('rank', DESCENDING)], limit=100):
            top_list.append({
                "id": str(objectid.ObjectId(item["_id"])),
                "rank": item["rank"],
                "date": item["date"],
                "up": item["up_vote"],
                "down": item["down_vote"],
                "post": item["post"]
            })

        return top_list
