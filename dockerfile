FROM python:2
MAINTAINER Yaron Pdut "yaronp@gmail.com"
EXPOSE 5000
WORKDIR /app
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY . .
CMD [ "python", "web/app.py" ]
